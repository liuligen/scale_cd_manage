import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const login = r => require.ensure([], () => r(require('@/page/login')), 'login');
const manage = r => require.ensure([], () => r(require('@/page/manage')), 'manage');
const home = r => require.ensure([], () => r(require('@/page/home')), 'home');
const addProject = r => require.ensure([], () => r(require('@/page/addProject')), 'addProject');
const userList = r => require.ensure([], () => r(require('@/page/userList')), 'userList');
const projectList = r => require.ensure([], () => r(require('@/page/projectList')), 'projectList');
const explain = r => require.ensure([], () => r(require('@/page/explain')), 'explain');
const repositoriesList = r => require.ensure([], () => r(require('@/page/repositoriesList')), 'repositoriesList');

const routes = [
	{
		path: '/',
		component: login
	},
	{
		path: '/manage',
		component: manage,
		name: '',
		children: [{
			path: '',
			component: home,
			meta: [],
		},{
			path: '/addProject',
			component: addProject,
			meta: ['添加数据', '添加项目'],
		},{
			path: '/userList',
			component: userList,
			meta: ['数据管理', '用户列表'],
		},{
			path: '/projectList',
			component: projectList,
			meta: ['数据管理', '项目列表'],
		},{
			path: '/explain',
			component: explain,
			meta: ['说明', '说明'],
		},{
            path: '/repositoriesList/:project_name',
            component: repositoriesList,
            meta: ['制成品仓库', '制成品仓库'],
        }]
	}
]

export default new Router({
	routes,
	strict: process.env.NODE_ENV !== 'production',
})
