import fetch from '@/config/fetch'

/**
 * 登陆
 */

export const login = data => fetch('/admin/login', data, 'POST');

/**
 * 退出
 */

export const signout = () => fetch('/admin/singout');



export const addProject = data => fetch('/projects', data, 'POST');

export const getProjects = data => fetch('/projects', data);

export const getResturantDetail = restaurant_id => fetch('/shopping/restaurant/' + restaurant_id);

export const getResturantsCount = () => fetch('/shopping/restaurants/count');

export const updateResturant = data => fetch('/shopping/updateshop', data, 'POST');

export const deleteResturant = restaurant_id => fetch('/shopping/restaurant/' + restaurant_id, {}, 'DELETE');


